FROM openjdk:11
WORKDIR /app
COPY target/crud_person-0.0.1-SNAPSHOT.jar /app/crud_person.jar
ENTRYPOINT ["java", "-jar", "crud_person.jar"]