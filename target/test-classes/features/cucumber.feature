Feature: CRUD Person

  Scenario: the user want create a person
    When the user sending a person to be created
      | firstName | lastName  | cpf            | rg           |
      | Lucas     | Freitas   | 234.567.987-76 | 67.654.854-7 |
    Then the person should be in the request of people who are registered

  Scenario: the user want get all people who are registered
    When the user request all people registered
    Then all the people registered are returned

  Scenario: the user want update a person who are registered
    When the user sending a person to be updated
      | firstName | lastName  | cpf            | rg           |
      | Lucas     | Furlam   | 456.123.573-25 | 29.159.653-9 |
    Then the person should be in the request of people who are registered

  Scenario: the user want delete a person who are registered
    When the user sends the person's id to be delete
    Then the person shouldn't be in the request of people who are registered