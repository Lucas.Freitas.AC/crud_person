package com.example.avenuecode.crud_person.controller;

import com.example.avenuecode.crud_person.model.Person;
import com.example.avenuecode.crud_person.service.PersonFactoryServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/people")
public class PersonController {

    private static final Logger logger = LoggerFactory.getLogger(PersonController.class);

    @Autowired
    private PersonFactoryServiceImpl personService;

    @GetMapping
    public List<Person> list() {
        logger.info("Init of the method List Person");

        return personService.list();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Person create(@RequestBody Person person) {
        logger.info("Init of the method Create Person with the request body {}", person);

        return personService.create(person);
    }

    @PutMapping("/{personId}")
    public Person update(@PathVariable String personId, @RequestBody Person person) {
        logger.info("Init of the method Update Person with the request body {} and param of id person {}", personId, person);

        Person actualPerson = personService.findPersonById(personId);

        BeanUtils.copyProperties(person, actualPerson, "id");

        logger.info("Response for the method Update Person: ", personService.create(actualPerson));

        return personService.create(actualPerson);
    }

    @DeleteMapping("/{personId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable String personId) {
        logger.info("Init of the method Delete Person with the param of id person {}", personId);

        personService.remove(personId);
    }

    @GetMapping("/person/{person-type}")
    public String getVehicleTypeInfo(@PathVariable("person-type") String person) throws Exception {

        com.example.avenuecode.crud_person.service.Person personFactory = PersonFactoryServiceImpl.createInstance(person);
        return personFactory.specification();

    }

}
