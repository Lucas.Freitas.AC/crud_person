package com.example.avenuecode.crud_person.service;

import com.example.avenuecode.crud_person.controller.PersonController;
import com.example.avenuecode.crud_person.model.Person;
import com.example.avenuecode.crud_person.repository.PersonRepository;
import com.example.avenuecode.crud_person.service.impl.Buddy;
import com.example.avenuecode.crud_person.service.impl.Developer;
import com.example.avenuecode.crud_person.service.impl.Manager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PersonFactoryServiceImpl {

    private static final Logger logger = LoggerFactory.getLogger(PersonController.class);

    @Autowired
    private Buddy buddy;

    @Autowired
    private Manager manager;

    @Autowired
    private Developer developer;

    private static final Map<String, com.example.avenuecode.crud_person.service.Person> handler = new HashMap<>();

    @Autowired
    private PersonRepository personRepository;

    @PostConstruct
    private Map<String, com.example.avenuecode.crud_person.service.Person> getObject() {
        handler.put("manager", manager);
        handler.put("buddy", buddy);
        handler.put("developer", developer);
        return handler;
    }

    public static com.example.avenuecode.crud_person.service.Person createInstance(String person) throws Exception {
        return Optional.ofNullable(handler.get(person)).orElseThrow(() -> new IllegalArgumentException("Invalid person"));
    }

    public List<Person> list() {

        logger.info("Response for the method List Person: {}", personRepository.findAll().toString());

        return personRepository.findAll();
    }

    public Person create(Person person) {
        logger.info("Response for the method Create Person: {}", personRepository.save(person));

        return personRepository.save(person);
    }

    public Person findPersonById(String personId) {
        return personRepository.findById(personId).orElseThrow();
    }

    public void remove(String personId) {
        personRepository.deleteById(personId);
    }

}
