package com.example.avenuecode.crud_person.service.impl;

import com.example.avenuecode.crud_person.service.Person;
import org.springframework.stereotype.Component;

@Component
public class Manager implements Person {
    @Override
    public String specification() {
        return "This is a Manager Person!!!";
    }
}
