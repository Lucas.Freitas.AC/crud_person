package com.example.avenuecode.crud_person.cucumber;

import com.example.avenuecode.crud_person.model.Person;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.Before;
import io.cucumber.java.DefaultDataTableCellTransformer;
import io.cucumber.java.DefaultDataTableEntryTransformer;
import io.cucumber.java.DefaultParameterTransformer;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.jupiter.api.Assertions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

@CucumberContextConfiguration
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class PersonStepsTest {

    private static final String URL = "http://localhost:8080/people";

    private RestTemplate restTemplate = new RestTemplate();

    private List<Person> actualPerson;

    private String personId;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @DefaultDataTableCellTransformer
    @DefaultDataTableEntryTransformer
    @DefaultParameterTransformer
    public Object transform(final Object from, final Type to) {
        return objectMapper.convertValue(from, objectMapper.constructType(to));
    }

    @Before
    public void setUp() {
        actualPerson = new ArrayList<>();
        personId = null;
    }

    @When("the user sending a person to be created")
    public void theUserSendingAPersonToBeCreated(Person person) {
        Person newPerson = restTemplate.postForObject(URL, person, Person.class);
        actualPerson.add(newPerson);
        personId = newPerson.getId();
        Assertions.assertNotNull(newPerson);
    }

    @Then("the person should be in the request of people who are registered")
    public void thePersonShouldBeInTheRequestOfPeopleWhoAreRegistered() {
        Person[] personList = restTemplate.getForObject(URL, Person[].class);

        List<Person> resultList = Arrays.stream(personList).filter(person -> person.getId().equals(personId)).collect(Collectors.toList());

        Assertions.assertEquals(1, resultList.size());

    }

    @When("the user request all people registered")
    public void theUserRequestAllPeopleRegistered() {
        Person[] personList = restTemplate.getForObject(URL, Person[].class);

        Arrays.stream(personList).forEach(person -> actualPerson.add(person));

        Assertions.assertNotNull(personList);
    }

    @Then("all the people registered are returned")
    public void allThePeopleRegisteredAreReturned() {
        Assertions.assertNotNull(actualPerson.size());
    }

    @When("the user sending a person to be updated")
    public void theUserSendingAPersonToBeUpdatedWithId(Person person) {
        Person[] personList = restTemplate.getForObject(URL, Person[].class);

        Map<String, String> params = new HashMap<>();
        params.put("personId", personList[0].getId());
        personId = personList[0].getId();

        HttpEntity<Person> httpEntity = new HttpEntity<>(person);
        Person updatedPerson = restTemplate.exchange(URL + "/{personId}", HttpMethod.PUT, httpEntity, Person.class, params).getBody();

        Assertions.assertNotNull(updatedPerson);
    }

    @When("the user sends the person's id to be delete")
    public void theUserSendsThePersonSIdToBeDelete() {
        Person[] personList = restTemplate.getForObject(URL, Person[].class);

        Map<String, String> params = new HashMap<>();
        params.put("personId", personList[0].getId());

        personId = personList[0].getId();

        restTemplate.delete(URL + "/{personId}", params);
    }

    @Then("the person shouldn't be in the request of people who are registered")
    public void thePersonShouldnTBeInTheRequestOfPeopleWhoAreRegistered() {
        Person[] personList = restTemplate.getForObject(URL, Person[].class);

        List<Person> resultList = Arrays.stream(personList).filter(person -> person.getId().equals(personId)).collect(Collectors.toList());

        Assertions.assertEquals(0, resultList.size());
    }
}
