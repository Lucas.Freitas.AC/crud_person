package com.example.avenuecode.crud_person.controller;

import com.example.avenuecode.crud_person.model.Person;
import com.example.avenuecode.crud_person.service.PersonFactoryServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(PersonController.class)
public class PersonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PersonFactoryServiceImpl personService;

    @Test
    public void shouldGetListPerson() throws Exception {
        List<Person> listPerson = new ArrayList<>();

        listPerson.add(new Person("1", "Lucas", "Furlam", "564.678.325-78", "67.876.654-9"));
        listPerson.add(new Person("1", "Rafael", "Romao", "456.123.765-92", "33.521.098-7"));

        Mockito.when(personService.list()).thenReturn(listPerson);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/people"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        String jsonResponse =  mvcResult.getResponse().getContentAsString();

        String expectedJsonResponse = objectMapper.writeValueAsString(listPerson);

        Assertions.assertEquals(jsonResponse, expectedJsonResponse);
    }

    @Test
    public void shouldCreatePerson() throws Exception {
        Person newPerson = new Person("Lucas", "Furlam", "564.678.325-78", "67.876.654-9");
        Person savedPerson = new Person("1", "Lucas", "Furlam", "564.678.325-78", "67.876.654-9");

        Mockito.when(personService.create(newPerson)).thenReturn(savedPerson);

        mockMvc.perform(MockMvcRequestBuilders.post("/people")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(newPerson)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(savedPerson)));
    }

    @Test
    public void shouldUpdatePerson() throws Exception {
        Person updatePerson = new Person("Lucas", "Freitas", "564.678.325-78", "67.876.654-9");
        Person savedPerson = new Person("1", "Lucas", "Freitas", "564.678.325-78", "67.876.654-9");

        Mockito.when(personService.findPersonById("1")).thenReturn(savedPerson);
        Mockito.when(personService.create(updatePerson)).thenReturn(savedPerson);

        mockMvc.perform(MockMvcRequestBuilders.put("/people/{personId}", "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatePerson)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void shouldRemovePerson() throws Exception {

        Mockito.doNothing().when(personService).remove("1");

        mockMvc.perform(MockMvcRequestBuilders.delete("/people/{personId}", "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Mockito.verify(personService, Mockito.times(1)).remove("1");
    }

}
