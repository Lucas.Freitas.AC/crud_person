package com.example.avenuecode.crud_person.controller;

import com.example.avenuecode.crud_person.model.Person;
import com.example.avenuecode.crud_person.repository.PersonRepository;
import com.example.avenuecode.crud_person.service.PersonFactoryServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonFactoryServiceImpl personService;

    @Test
    public void shouldGetListPerson() throws Exception {
        personRepository.deleteAll();
        List<Person> listPerson = new ArrayList<>();

        listPerson.add(new Person("1", "Lucas", "Furlam", "564.678.325-78", "67.876.654-9"));
        listPerson.add(new Person("2", "Rafael", "Romao", "456.123.765-92", "33.521.098-7"));

        personService.create(new Person("1", "Lucas", "Furlam", "564.678.325-78", "67.876.654-9"));
        personService.create(new Person("2", "Rafael", "Romao", "456.123.765-92", "33.521.098-7"));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/people"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        String jsonResponse =  mvcResult.getResponse().getContentAsString();

        String expectedJsonResponse = objectMapper.writeValueAsString(listPerson);

        List<Person> personListResult = personService.list();

        Assertions.assertEquals(2, personListResult.size());
        Assertions.assertEquals(jsonResponse, expectedJsonResponse);
    }

    @Test
    public void shouldCreatePerson() throws Exception {
        Person newPerson = new Person("Lucas", "Furlam", "564.678.325-78", "67.876.654-9");

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/people")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(newPerson)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated()).andReturn();

        Person person = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Person.class);

        Person personReturn = personRepository.findById(person.getId()).orElseThrow();

        Assertions.assertEquals(person, personReturn);
    }

    @Test
    public void shouldUpdatePerson() throws Exception {
        Person updatePerson = new Person("Lucas", "Freitas", "564.678.325-78", "67.876.654-9");

        personService.create(new Person("1", "Lucas", "Furlam", "564.678.325-78", "67.876.654-9"));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/people/{personId}", "1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatePerson)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        Person person = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Person.class);

        Person personReturn = personRepository.findById(person.getId()).orElseThrow();

        Assertions.assertEquals(person, personReturn);
    }

    @Test
    public void shouldRemovePerson() throws Exception {

        personService.create(new Person("1", "Lucas", "Furlam", "564.678.325-78", "67.876.654-9"));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete("/people/{personId}", "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNoContent()).andReturn();

        Optional<Person> personResult = personRepository.findById("1");

        String response = mvcResult.getResponse().getContentAsString();

        Assertions.assertEquals(personResult.isEmpty() ? "" : personResult, response);

    }
}
